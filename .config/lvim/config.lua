-- Custom plugins
lvim.plugins = {
  -- One dark theme
  {
    'navarasu/onedark.nvim',
    config = function()
      require('onedark').setup {
        style = 'darker',

        -- Making it transparent through lvim's settings didnt work
        transparent = true,
        lualine = {
          transparent = true,
        },
      }
    end
  },
  { "christoomey/vim-tmux-navigator" },
}

-- Disable Run and Debug the main function
require('lspconfig').rust_analyzer.setup{
  settings = {
    ['rust-analyzer'] = {
      hover = {
        actions = {
          debug = { enable = false },
          run = { enable = false }
}}}}}

require'lspconfig'.textlsp.setup{}

-- Disable DAP
lvim.builtin.dap.active = false

-- Setup tmux navigation keys
lvim.keys.normal_mode["<C-h>"] = ":TmuxNavigateLeft<CR>"
lvim.keys.normal_mode["<C-l>"] = ":TmuxNavigateRight<CR>"
lvim.keys.normal_mode["<C-j>"] = ":TmuxNavigateDown<CR>"
lvim.keys.normal_mode["<C-k>"] = ":TmuxNavigateUp<CR>"

-- Misc keybinds
lvim.keys.normal_mode["ff"] = ":Telescope find_files<CR>"
lvim.keys.normal_mode["gr"] = "<Cmd>lua vim.lsp.buf.rename()<CR>"

-- Theme
lvim.colorscheme = "onedark"

-- Vim settings
vim.opt.relativenumber = true
vim.opt.wrap = true

vim.wo.scrolloff = 12
