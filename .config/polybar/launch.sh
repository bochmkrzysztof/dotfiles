#!/usr/bin/env bash

DIR="$HOME/.config/polybar"

polybar-msg cmd quit
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar top1 -c "$DIR"/config.ini &
polybar top2 -c "$DIR"/config.ini &
