#!/bin/bash

# Set the callback function to be executed when a line is received
callback_function() {
    echo "Received line: $1"
    # Add your custom logic here
    # For example, you can run a command or perform some actions based on the received line
}

# Function to clean up and delete the FIFO file
cleanup() {
    echo "Cleaning up and deleting FIFO file: $fifo_file"
    rm -f "$fifo_file"
    exit 0
}

# Create a FIFO file in the XDG runtime directory
fifo_file="$XDG_RUNTIME_DIR/myfifo"
if [[ ! -p "$fifo_file" ]]; then
    echo "Creating FIFO file: $fifo_file"
    mkfifo "$fifo_file"
fi

# Set the trap to catch script termination signals and run the cleanup function
trap cleanup EXIT

# Read lines from the FIFO file and execute the callback function
while read -r line; do
    callback_function "$line"
done < "$fifo_file"
