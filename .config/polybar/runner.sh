#!/bin/bash

handle_signal() {
  DIR="$HOME/.config/polybar"

  polybar-msg cmd quit
  while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

  polybar -q top1 -c "$DIR"/config.ini &
  polybar -q top2 -c "$DIR"/config.ini &
}

handle_exit() {
  rm "$pid_file"
}

# Run it at the beginning too
handle_signal

trap 'handle_signal' SIGUSR1
trap 'handle_exit' EXIT

pid_file="$XDG_RUNTIME_DIR/polybar-runner.pid"

if ! echo "$$" > "$pid_file"; then
  echo "Failed to create PID file: $pid_file"
  exit 1
fi

while true; do sleep 1; done
