return {
  "neovim/nvim-lspconfig",
  opts = {
    servers = {
      pyright = {
        enabled = true,
      },
      rust_analyzer = {},
      clangd = {},
      nushell = {},
    },
  },
}
