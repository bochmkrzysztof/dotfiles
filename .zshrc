if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    # nop
else
    if [ "$TMUX" = "" ]; then exec tmux; fi
fi


HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

setopt share_history
autoload -U compinit
compinit


function mcd()
{
    mkdir -p -- "$1" &&
    cd -P -- "$1"
}

function nt()
{
    alacritty --working-directory $PWD &
    disown
}

# basic
alias ls='eza --icons always'
alias grep='grep --color=auto'
alias q='exit'
alias cp='cp -v'
alias dirs='dirs -lvp'
alias cat='bat -p'
alias tmpcd='cd $(mktemp -d)'

# doas
#alias docker='sudo docker'

# nvim
alias v='nvim'
alias vi='nvim'
alias vim='nvim'
#alias nvim='lvim'

alias sploit='tar xvfkz ~/Templates/sploit.tar.gz'
alias rr='source ~/.zshrc'

# other
alias scan='scanimage --format=png --progress --output-file'

alias wifi-ls='nmcli device wifi rescan; nmcli device wifi list'
alias wifi-cn='nmcli device wifi connect'
alias wifi-home='wifi-cn D8:D7:75:22:2D:4A'

alias ip.me='curl ip.me'

alias bt-hp='bluetoothctl connect 64:68:76:40:3F:FB'
alias bt-sn='bluetoothctl connect AC:80:0A:75:04:85'

alias cn-lab='nmcli connection up 4dac91d4-8805-4d56-baae-eb077b48b6e5'
alias dc-lab='nmcli connection down 4dac91d4-8805-4d56-baae-eb077b48b6e5'

alias unmount-disk='udisksctl unmount -b /dev/disk/by-id/dm-name-luks-49194771-e5dd-4076-bc60-a827d58300c7 && udisksctl lock -b /dev/disk/by-id/usb-Samsung_PSSD_T7_S6XDNS0W351055Z-0:0-part1 && udisksctl power-off -b /dev/disk/by-id/usb-Samsung_PSSD_T7_S6XDNS0W351055Z-0:0'

# gitc
alias gitc='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
function gitcc() {
    local name="$(date '+Dotfiles: %d.%m.%Y %T')"
    gitc commit -m "$name"
}
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Ghidra is bugged :(
alias ghidra='_JAVA_AWT_WM_NONREPARENTING=1 ghidra'

#export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export PATH="$PATH:$HOME/.local/bin"

echo -e -n ""
#[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

#eval "$(zoxide init zsh)"
eval "$(starship init zsh)"
#eval $(ssh-agent)
