#!/bin/sh

if pidof i3lock; then exit; fi

BLANK='#00000000'
CLEAR='#ffffff22'
DEFAULT='#0000ffcc'
TEXT='#0000eeee'
WRONG='#880000bb'
VERIFYING='#0000bbbb'
INSIDE='#9999ff66'

i3lock \
--color='#000000'         \
--insidever-color=$CLEAR     \
--ringver-color=$VERIFYING   \
\
--insidewrong-color=$CLEAR   \
--ringwrong-color=$WRONG     \
\
--inside-color=$INSIDE        \
--ring-color=$DEFAULT        \
--line-color=$BLANK          \
--separator-color=$DEFAULT   \
\
--verif-color=$TEXT          \
--wrong-color=$TEXT          \
--time-color=$TEXT           \
--date-color=$TEXT           \
--layout-color=$TEXT         \
--keyhl-color=$WRONG         \
--bshl-color=$WRONG          \
\
--radius 200                \
--ring-width 15                \
\
--ignore-empty-password      \
--show-failed-attempts       \
--screen 1                   \
--clock                      \
--indicator                  \
--time-str="%H:%M:%S"        \
--date-str="%A, %Y-%m-%d"       \
--keylayout 1               \
\
--pass-media-keys           \
--pass-volume-keys          \
--nofork
#--no-verify

